// raylib-zig (c) Nikolas Wipper 2023

const rl = @import("raylib");

const Sprite = struct {
    position: rl.Vector2,
    origin: rl.Vector2,
    texture: *const rl.Texture,
    sourceRect: rl.Rectangle,
    scale: f32,
    rotation: f32,

    pub fn draw(self: Sprite) void {
        const destRec = rl.Rectangle.init(
            self.position.x,
            self.position.y,
            self.sourceRect.width * self.scale,
            self.sourceRect.height * self.scale,
        );
        self.texture.drawPro(self.sourceRect, destRec, self.origin, self.rotation, rl.Color.white);
    }
};

pub fn main() anyerror!void {
    const screenWidth = 800;
    const screenHeight = 450;

    rl.initWindow(screenWidth, screenHeight, "raylib-zig [core] example - basic window");
    defer rl.closeWindow(); // Close window and OpenGL context

    const spritesheet: rl.Texture = rl.Texture.init("resources/textures/spritesheet.png");
    defer rl.unloadTexture(spritesheet);

    var player: Sprite = Sprite{
        .position = rl.Vector2.init(350.0, 280.0),
        .origin = rl.Vector2.init(0.0, 0.0),
        .texture = &spritesheet,
        .sourceRect = rl.Rectangle.init(0, 0, 32, 32),
        .scale = 2.0,
        .rotation = 0.0,
    };

    rl.setTargetFPS(60); // Set our game to run at 60 frames-per-second

    // Main game loop
    while (!rl.windowShouldClose()) { // Detect window close button or ESC key

        rl.beginDrawing();
        defer rl.endDrawing();

        rl.clearBackground(rl.Color.white);

        if (rl.isKeyDown(rl.KeyboardKey.key_left)) {
            player.position.x -= 32.0 / 60.0;
        }

        if (rl.isKeyDown(rl.KeyboardKey.key_right)) {
            player.position.x += 32.0 / 60.0;
        }

        if (rl.isKeyDown(rl.KeyboardKey.key_up)) {
            player.position.y -= 32.0 / 60.0;
        }

        if (rl.isKeyDown(rl.KeyboardKey.key_down)) {
            player.position.y += 32.0 / 60.0;
        }

        player.draw();
    }
}
